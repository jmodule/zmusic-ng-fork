#!/bin/bash
# https://git.zx2c4.com/zmusic-ng/about/
uwsgi --chdir backend/ -w zmusic:app --http-socket 0.0.0.0:5000 --master --processes 4 --threads 2
