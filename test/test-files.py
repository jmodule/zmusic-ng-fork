from __future__ import print_function
import os
import time
import sys
sys.path.insert(0, './backend')
from zmusic.picard.util import encode_filename

MUSIC_PATH = "/home/jfriant80/Music"


def test_scan_files():
    for root, dirs, files in os.walk(unicode(MUSIC_PATH)):
        if len(files) != 0:
            yield "%i | Scanning [%s].\n" % (int(time.time()), encode_filename(root))
            for name in files:
                name = encode_filename(os.path.join(root, name))
                yield "%i | Encoded Path [%s].\n" % (int(time.time()), name)


if __name__ == "__main__":
    # Testing to see how the encoded path looks when there are characters outside the normal ASCII set.
    for line in test_scan_files():
        print(line, end="")
